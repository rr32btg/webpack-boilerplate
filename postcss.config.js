module.exports = {
  plugins: [
    require('cssnano')({zindex: false}),
    require('postcss-cssnext')({
      browsers: ['last 2 versions', 'ie >= 9'],
      compress: true
    }),
    require('postcss-smart-import'),
    require("postcss-url")
  ]
};
