const path = require('path');
const merge = require('webpack-merge');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const config = require('./webpack.config.base');

const GLOBALS = {
    'process.env': {
        'NODE_ENV': JSON.stringify('production')
    },
    __DEV__: JSON.stringify(JSON.parse(process.env.DEBUG || false))
};

let entry_point = [];
const entry_dir = fs.existsSync(__dirname + '/src') ? __dirname + '/src' : __dirname + '/example';

if (fs.existsSync(__dirname + '/src/index.js')) {
    entry_point.push(__dirname + '/src/index.js');
}else{
    entry_point.push(__dirname + '/example/index.js');
}

module.exports = merge(config, {
    // devtool: 'source-map',
    entry: {
        app: entry_point,
        vendor: ['react', 'react-router', 'react-redux', 'redux']
    },
    "resolve": {
        "alias": {
          "react": "preact-compat",
          "react-dom": "preact-compat"
        }
        // alias: {
        //   'react': 'inferno-compat',
        //   'react-dom': 'inferno-compat'
        // }
    },
    plugins: [
        new CopyWebpackPlugin([
            {
                from: path.join(entry_dir, '/index.html'),
                to: path.join(__dirname, './dist/index.html')
            }
        ]),
        new webpack.DefinePlugin(GLOBALS),
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false,
                screw_ie8: true,
                conditionals: true,
                unused: true,
                comparisons: true,
                sequences: true,
                dead_code: true,
                evaluate: true,
                join_vars: true,
                if_return: true
            },
            output: {
                comments: false
            },
            sourceMap: true
        }),
        new webpack.LoaderOptionsPlugin({
            minimize: true,
            debug: false
        }),
        new ExtractTextPlugin({
            filename: 'css/app.css',
            allChunks: true
        }),
    ],
    module: {
        noParse: /\.min\.js$/,
        rules: [
            // Sass + CSS Modules
            {
              test: /\.?a?c?ss$/,
              use: ExtractTextPlugin.extract({
                fallback: 'style-loader',
                use: [{
                    loader: 'css-loader',
                    options: {
                      modules: true,
                      importLoaders: 1,
                      localIdentName: '[path][name]__[local]--[hash:base64:5]'
                    }
                  },
                  'postcss-loader',
                  {
                    loader: 'sass-loader',
                    options: {
                      outputStyle: 'compressed'
                    }
                  }]
              })
            },
            {
              test: /\.less$/,
              use: ExtractTextPlugin.extract({
                fallback: 'style-loader',
                use: [{
                  loader: 'css-loader',
                  options: {
                    modules: true,
                    importLoaders: 1,
                    localIdentName: '[path][name]__[local]--[hash:base64:5]'
                  }
                },
                  'postcss-loader',
                  'less-loader'
                ]
              })
            }
        ]
    },
});
