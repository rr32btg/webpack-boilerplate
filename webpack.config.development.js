const path = require('path');
const fs = require('fs');
const merge = require('webpack-merge');
const webpack = require('webpack');
const config = require('./webpack.config.base');

const GLOBALS = {
    'process.env': {
        'NODE_ENV': JSON.stringify('development')
    },
    __DEV__: JSON.stringify(JSON.parse(process.env.DEBUG || 'true'))
};

let entry_point = ['webpack-hot-middleware/client', 'react-hot-loader/patch'];

if (fs.existsSync(__dirname + '/src/index.js')) {
    entry_point.push(__dirname + '/src/index.js');
}else{
    entry_point.push(__dirname + '/example/index.js');
}

module.exports = merge(config, {
    cache: true,
    devtool: '#cheap-eval-source-map',
    entry: {
        app: entry_point,
        vendor: ['react', 'react-dom', 'react-router', 'react-redux', 'redux']
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.DefinePlugin(GLOBALS),
        // new CopyWebpackPlugin([
        //     {
        //         from: path.join(__dirname, './src/mocks/'),
        //         to: 'mocks'
        //     }
        // ]),
    ],
    module: {
        rules: [
            // Sass + CSS Modules
            {
                test: /\.?a?c?ss$/,
                use: [
                    'style-loader',
                    {
                        loader: 'css-loader',
                        options: {
                            modules: true,
                            importLoaders: 1,
                            localIdentName: '[path][name]__[local]--[hash:base64:5]'
                        }
                    },
                    'postcss-loader',
                    {
                      loader: 'sass-loader',
                      options: {
                        outputStyle: 'expanded'
                      }
                    }
                ]
            },
            // Less + css modules
            {
                test: /\.less$/,
                use: [
                    'style-loader',
                    {
                        loader: 'css-loader',
                        options: {
                            modules: true,
                            importLoaders: 1,
                            localIdentName: '[path][name]__[local]--[hash:base64:5]'
                        }
                    },
                    'postcss-loader',
                    'less-loader'
                ]
            }
        ]
    }
});
