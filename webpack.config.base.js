// base config used by development/production

const path = require('path');
const fs = require('fs');
const webpack = require('webpack');

module.exports = {
    output: {
        filename: 'js/[name].js',
        path: __dirname + '/dist',
        publicPath: '/'
    },
    resolve: {
        modules: [
            'node_modules'
        ],
        extensions: ['.js', '.jsx', '.json', '.scss']
    },
    plugins: [
        new webpack.ProvidePlugin({
            'fetch': 'imports-loader?this=>global!exports-loader?global.fetch!whatwg-fetch'  // fetch API
        }),
        // Shared code
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor',
            filename: 'js/vendor.js',
            minChunks: Infinity
        })
    ],
    module: {
        rules: [
            // JavaScript / ES6
            {
                test: /\.jsx?$/,
                include: fs.existsSync(__dirname + '/src') ? __dirname + '/src' : __dirname + '/example',
                loader: 'babel-loader',
                query: {
                    cacheDirectory: true
                }
            },
            // Images
            // Inline base64 URLs for <=8k images, direct URLs for the rest
            {
                test: /\.(png|jpg|jpeg|gif|svg)$/,
                loader: 'url-loader',
                options: {
                    limit: 8192,
                    name: 'images/[name].[ext]?[hash]'
                }
            },
            // Fonts
            {
                test: /\.(woff|woff2|ttf|eot)(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'url-loader',
                options: {
                    limit: 8192,
                    name: 'fonts/[name].[ext]?[hash]'
                }
            },
            // Html
            {
                test: /\.html$/,
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]'
                }
            },
            // video
            {
                test: /\.mp4$/,
                loader: 'file-loader',
                options: {
                    name: 'video/[name].[ext]'
                }
            },
            //graphQL
            {
              test: /\.graphQL$/,
              use: 'raw-loader'
            },
            {
              test: /\.json$/,
              use: 'json-loader'
            }

        ]
    }
};
