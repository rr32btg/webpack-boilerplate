/**
 * @flow
 * Created by rr32btg on 1 марта 2017 г.
 */

import redux from 'redux';

import * as actions from './actions';
import * as constants from './consts';

import type {Reducer} from 'redux';
// import type {Action} from '../types.js';

type mainState = {
  counter: number
};

const initialAuthState: mainState = {
  counter: 0
};

export function mainReducer(state: mainState = initialAuthState, action: Action){

  let type = action.type;
  let payload = action.payload;

  switch (type) {
    case constants.COUNTER_UP:
      return {...state, counter: state.counter+1};

    default:
      return state;
  }

}
