/**
 * Created by rr32btg on 26.03.17.
 */

import * as actions from './actions';
import {bindActionCreators} from 'redux';

export function mapStateToProps(state){
    return state.mainReducer;
}

export function mapDispatchToProps(dispatch){
    return {do: bindActionCreators(actions, dispatch)};
}