/**
 * @flow
 * Created by rr32btg on 1 марта 2017 г.
 */

import * as actions from './consts';

export function counterUP() {
  return {type: actions.COUNTER_UP};
}
