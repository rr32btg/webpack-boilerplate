/**
 * Created by rr32btg on 26.03.17.
 */

import React, {Component} from 'react';
import {connect} from 'react-redux';

import {mapStateToProps, mapDispatchToProps} from '../../store/reducers/main/mapper';

import s from './Example.sass';

class Example extends Component{
    render(){
        return (
            <div>
                <p className={s.someClass}>counter: {this.props.counter}</p>
                <button onClick={this.props.do.counterUP}>+1</button>
            </div>
        );
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Example);