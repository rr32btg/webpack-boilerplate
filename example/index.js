/**
 * @flow
 * Created by rr32btg on 1 марта 2017 г.
 */

import React from 'react';
import ReactDOM from 'react-dom';

import Routes from './routes';

const rootEl = document.getElementById('app');

//FIX when rendered with preact-compat rootEl stay dirty if not do that
rootEl.innerHTML = '';


ReactDOM.render(<Routes />, rootEl,);

if (__DEV__ && !!module.hot) {
    function hot(){
        console.log('dev active');

        const AppContainer = require('react-hot-loader').AppContainer;
        const RedBox = require('redbox-react').default;

        ReactDOM.render((
            <AppContainer errorReporter={RedBox}>
                <Routes />
            </AppContainer>
        ), rootEl);
    }

    module.hot.accept('./routes', hot);

}
