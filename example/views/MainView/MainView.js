/**
 * Created by rr32btg on 26.03.17.
 */

import React from 'react';

import Header from '../../components/Header';
import Footer from '../../components/Footer';
import Example from '../../components/Example';

import s from './MainView.sass';

const MainView = () => (
    <div className={s.main_block}>
        <Header/>
        <Example/>
        <Footer/>
    </div>
);

export default MainView;