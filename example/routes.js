/**
* @flow
* Created by rr32btg on 1 марта 2017 г.
*/

import React, {Component} from 'react';
import {Provider} from 'react-redux';
import {BrowserRouter as Router, Route, IndexRoute, Link} from 'react-router-dom';
import {syncHistoryWithStore} from 'react-router-redux';
import createBrowserHistory from 'history/createBrowserHistory';

import MainView from './views/MainView';

import Store from './store';

const history = syncHistoryWithStore(createBrowserHistory(), Store);


export default class Routes extends Component{
  render(){
    return(
        <Provider store={Store}>
          <Router history={history}>
            <div>
              <Route exact path="/" component={MainView}/>
            </div>
          </Router>
        </Provider>
    );
  }
}
