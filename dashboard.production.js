/**
 * Created by rr32btg on 07.05.17.
 */

const path = require('path');
const fs = require('fs');
const express = require('express');
const webpack = require('webpack');
const webpackDevMiddleware = require('webpack-dev-middleware');
const DashboardPlugin = require('webpack-dashboard/plugin');
const config = require('./webpack.config.production');

const app = express();
const compiler = webpack(config);

// add dashboard
compiler.apply(new DashboardPlugin());

const host = process.env.HOST || '0.0.0.0';
const port = process.env.PORT || 3000;

function log() {
    arguments[0] = '\nWebpack: ' + arguments[0];
    console.log.apply(console, arguments);
}

app.use(webpackDevMiddleware(compiler, {
    noInfo: true,
    publicPath: config.output.publicPath,
    stats: {
        colors: true
    },
    historyApiFallback: true
}));

app.use('/mocks', express.static('mocks'));

app.get('*', (req, res) => {
    res.sendFile(path.join(fs.existsSync(__dirname + "/src/index.html") ? __dirname + '/src' : __dirname + '/example', '/index.html'));
});

app.listen(port, host, (err) => {
    if (err) {
        log(err);
        return;
    }

    log('🚧  App is listening at http://%s:%s', host, port);
});
