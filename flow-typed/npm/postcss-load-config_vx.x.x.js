// flow-typed signature: e38021e69f7fdcb915888f693226d56e
// flow-typed version: <<STUB>>/postcss-load-config_v^1.2.0/flow_v0.40.0

/**
 * This is an autogenerated libdef stub for:
 *
 *   'postcss-load-config'
 *
 * Fill this stub out by replacing all the `any` types.
 *
 * Once filled out, we encourage you to share your work with the 
 * community by sending a pull request to: 
 * https://github.com/flowtype/flow-typed
 */

declare module 'postcss-load-config' {
  declare module.exports: any;
}

/**
 * We include stubs for each file inside this npm package in case you need to
 * require those files directly. Feel free to delete any files that aren't
 * needed.
 */


// Filename aliases
declare module 'postcss-load-config/index' {
  declare module.exports: $Exports<'postcss-load-config'>;
}
declare module 'postcss-load-config/index.js' {
  declare module.exports: $Exports<'postcss-load-config'>;
}
